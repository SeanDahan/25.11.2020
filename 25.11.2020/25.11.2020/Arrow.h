#pragma once
#include "Polygon.h"

class Arrow: public Shape
{
public:
	Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name);
	~Arrow();

	// There's a need to enter an opposite Y when moving an arrow.
	virtual void move(const Point& other);

	virtual double getPerimeter() const ;
	virtual double getArea() const ;

	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);

private:
	std::vector<Point> _points;
};

