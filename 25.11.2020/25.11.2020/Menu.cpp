#include "Menu.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Circle.h"
#include <time.h>
#include <stdlib.h>
#include <iostream>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;

Menu::Menu()
{ }

Menu::~Menu()
{ }

unsigned short Menu::displayMenu()
{
	unsigned short ansr;
	do
	{
		system("cls");
		
		setColor(LIGHT_YELLOW);
		std::cout << "< Super Drawer V.2 >\n\n";
		setColor(RED);
		std::cout << "//Select\\\n";
		setColor(LIGHT_GREEN);
		
		std::cout << "[ Add a Shape ] - 0\n";
		std::cout << "[ Modify a Shape ] - 1\n";
		std::cout << "[ Del all Shapes ] - 2\n";
		std::cout << "[ Exit ] - 3\n";
		setColor(WHITE);
		std::cin >> ansr;
	} while (ansr < 0 || ansr > 3);
	return ansr;
}


void Menu::displayShapeMenu()
{
	unsigned short choice;
	do
	{
		system("cls");
		setColor(LIGHT_RED);
		std::cout << "> Shapes <\n\n";
		setColor(AQUA);
		std::cout << "[ Circle ] - 0\n";
		std::cout << "[ Arrow ] - 1\n";
		std::cout << "[ Triangle ] - 2\n";
		std::cout << "[ Rectangle ] - 3\n";
		setColor(WHITE);
		cin >> choice;
	} while (choice < 0 || choice > 3);
	
	switch (choice)
	{
	case 0:
		addCircle();
		break;
	case 1:
		addArrow();
		break;
	case 2:
		addTriangle();
		break;
	case 3:
		addRectangle();
		break;
	}
}

void Menu::addCircle()
{
	double x, y, radius;
	string name;
	setColor(PURPLE);
	std::cout << "{} Settings {}\n";
	setColor(GRAY);
	std::cout << "* Circle *\n";
	setColor(LIGHT_YELLOW);
	std::cout << "[ Please enter X ]\n";
	setColor(WHITE);
	std::cin >> x;
	setColor(LIGHT_YELLOW);
	cout << "[ Please enter Y ]\n";
	setColor(WHITE);
	std::cin >> y;
	setColor(LIGHT_YELLOW);
	cout << "[ Please enter radius ]\n";
	setColor(WHITE);
	std::cin >> radius;
	setColor(LIGHT_YELLOW);
	cout << "[ Please enter the name of the shape ]\n";
	setColor(PINK);
	std::cin >> name;

	Circle* c = new Circle(Point(x, y), radius, "Circle", name);
	shapes.push_back(c);

	srand(time(NULL));
	c->draw(_canvas);
}

void Menu::addArrow()
{
	string name;
	vector<Point> points;
	double x, y;
	setColor(PURPLE);
	std::cout << "{} Settings {}\n";
	setColor(GRAY);
	std::cout << "* Arrow *\n";
	
	for (int i = 0; i < 2; i++) {
		setColor(LIGHT_YELLOW);
		cout << "[ Enter the X of point number ]" << i + 1 << "\n";
		setColor(WHITE);
		cin >> x;
		setColor(LIGHT_YELLOW);
		cout << "[ Enter the Y of point number ]" << i + 1 << "\n";
		setColor(WHITE);
		cin >> y;
		points.push_back(Point(x, y));
	}

	setColor(LIGHT_YELLOW);
	cout << "[ Enter the name of the shape ]\n";
	setColor(PINK);
	cin >> name;

	
	Arrow* a = new Arrow(points[0], points[1], "Arrow", name);
	shapes.push_back(a);

	srand(time(NULL));
	a->draw(_canvas);
	
}

void Menu::addTriangle()
{
	vector<Point> points;
	string name;
	double x, y;
	setColor(PURPLE);
	std::cout << "{} Settings {}\n";
	setColor(GRAY);
	std::cout << "* Triangle *\n";
	for (int i = 0; i < 3; i++) {
		setColor(LIGHT_YELLOW);
		cout << "[ Enter the X of point number ]" << i + 1 << "\n";
		setColor(WHITE);
		cin >> x;
		setColor(LIGHT_YELLOW);
		cout << "[ Enter the Y of point number ]" << i + 1 << "\n";
		setColor(WHITE);
		cin >> y;
		points.push_back(Point(x, y));

		points.push_back(Point(x, y));
	}
	setColor(LIGHT_YELLOW);
	cout << "[ Enter the name of the shape ]\n";
	setColor(PINK);
	cin >> name;

	Triangle* t = new Triangle(points[0], points[1], points[2], "Triangle", name);
	shapes.push_back(t);

	srand(time(NULL));
	t->draw(_canvas);
}

void Menu::addRectangle()
{
	string name;
	double x, y;
	double length, width;

	setColor(PURPLE);
	std::cout << "{} Settings {}\n";
	setColor(GRAY);
	std::cout << "* Rectangle *\n";
	setColor(LIGHT_YELLOW);
	std::cout << "[ Enter the X of the to left corner ]\n";
	setColor(WHITE);
	std::cin >> x;
	setColor(LIGHT_YELLOW);
	cout << "[ Enter the Y of the top left corner ]\n";
	setColor(WHITE);
	std::cin >> y;
	setColor(LIGHT_YELLOW);
	cout << "[ Please enter the length of the shape ]\n";
	setColor(WHITE);
	std::cin >> length;
	setColor(LIGHT_YELLOW);
	cout << "[ Please enter the width of the shape ]\n";
	setColor(WHITE);
	std::cin >> width;
	setColor(LIGHT_YELLOW);
	cout << "[ Please enter the name of the shape ]\n";
	setColor(PINK);
	std::cin >> name;
	

	myShapes::Rectangle* r = new myShapes::Rectangle(Point(x, y), length, width, "Rectangle", name);
	shapes.push_back(r);

	srand(time(NULL));
	r->draw(_canvas);
}


void Menu::displayShape()
{
	if (shapes.size() == 0)
		return;
	unsigned short choice;
	do
	{
		system("cls");
		setColor(GREEN);
		std::cout << "< Shapes >\n";
		setColor(RED);
		std::cout << "//Select\\\n";
		setColor(LIGHT_BLUE);
		for (size_t i = 0; i < shapes.size(); i++)
		{
			std::cout << "[ " << shapes[i]->getName() << " { " << shapes[i]->getType() << " }] - "<<i << "\n";
		}
		
		setColor(WHITE);
		cin >> choice;
	} while (choice < 0 || choice >= shapes.size());
	displayModifyMenu(choice);
}
void Menu::displayModifyMenu(unsigned short index)
{
	unsigned short choice;
	do
	{
		setColor(GREEN);
		std::cout << "< Modifacations >\n";
		setColor(RED);
		std::cout << "//Select\\\n";
		setColor(LIGHT_YELLOW);
		std::cout << "[ Move Shape ] - 0\n";
		std::cout << "[ Print Details ] - 1\n";
		std::cout << "[ Delete Shape ] - 2\n";
		setColor(WHITE);
		cin >> choice;
	} while (choice < 0 || choice > 3);

	switch (choice)
	{
		case 0: 
			moveShapeMenu(index); 
			break;
		case 1: 
			shapes[index]->printDetails(); 
			system("PAUSE"); 
			break;
		case 2: 
			removeShape(index, true); 
			break;
	}
}

void Menu::moveShapeMenu(unsigned short index)
{
	double x, y;
	system("cls");
	setColor(PURPLE);
	std::cout << "{} Settings {}";
	setColor(RED);
	std::cout << "//Select\\\n";
	setColor(LIGHT_YELLOW);
	std::cout << "[ Please enter the X moving scale ]\n";
	setColor(WHITE);
	std::cin >> x;
	setColor(LIGHT_YELLOW);
	std::cout << "[ Please enter the Y moving scale ]\n";
	setColor(WHITE);
	std::cin >> y;

	removeShape(index, false);
	shapes[index]->move(Point(x, y));
	shapes[index]->draw(_canvas);
}

void Menu::removeShape(unsigned short index, bool delete_from_shapes)
{
	for (int i = 0; i < shapes.size(); i++)
	{
		// Print everything back again but the wanted index.
		if (i == index)
		{
			shapes[i]->clearDraw(_canvas);
		}
	}

	for (int i = 0; i < shapes.size(); i++)
	{
		// Print everything back again but the wanted index.
		if (i != index)
		{
			shapes[i]->draw(_canvas);
		}
	}

	if (delete_from_shapes)
		shapes.erase(shapes.begin() + index);

}

void Menu::deleteAll()
{
	for (size_t i = 0; i < shapes.size(); i++)
	{
		shapes[i]->clearDraw(_canvas);
	}
	shapes.erase(shapes.begin(), shapes.end());
}

void Menu::setColor(unsigned int color)
{
	HANDLE hConsole;
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, color);
}

