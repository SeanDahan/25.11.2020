#pragma once
#include "Shape.h"
#include <vector>
#include "Canvas.h"

#define RED 4
#define GREEN 2
#define PURPLE 5
#define GRAY 8
#define LIGHT_GREEN 10
#define AQUA 11
#define LIGHT_RED 12
#define PINK 13
#define YELLOW 6
#define LIGHT_YELLOW 14
#define LIGHT_BLUE 9
#define WHITE 15

class Menu
{
public:
	
	
	
	

	Menu();
	~Menu();

	//Add shape
	void addCircle();
	void addArrow();
	void addTriangle();
	void addRectangle();

	
	//Add ons
	enum MainMenu { addChoice, modifyChoice, delChoice, exitChoice };

	//Display
	unsigned short displayMenu();
	void displayShapeMenu();
	void displayModifyMenu(unsigned short index);
	void displayShape();
	

	
	//Other
	
	

	void moveShapeMenu(unsigned short index);
	void removeShape(unsigned short index, bool delete_from_shapes);
	void deleteAll();

	//Helpers
	void setColor(unsigned int color);
private: 
	Canvas _canvas;
	std::vector<Shape*> shapes;
};

