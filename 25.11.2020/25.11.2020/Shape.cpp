#include "Shape.h"

#include <iostream>

Shape::Shape(const std::string & name, const std::string & type) : _name(name), _type(type)
{}

void Shape::printDetails() const
{
	setColor(AQUA);
	std::cout << "[ Name ]\t";
	setColor(WHITE);
	std::cout << _name.c_str() << "\n";
	setColor(AQUA);
	std::cout << "[ Type ]\t";
	setColor(WHITE);
	std::cout << _type.c_str() << "\n";//Feels like Python
	setColor(AQUA);
	std::cout << "[ Area ]\t";
	setColor(WHITE);
	std::cout << getArea() << "\n";
	setColor(AQUA);
	std::cout << "[ Perimeter ]\t";
	setColor(WHITE);
	std::cout << getPerimeter() << "\n";
}


std::string Shape::getType() const
{ 
	return _type; 
}

std::string Shape::getName() const
{ 
	return _name; 
}

void Shape::setColor(unsigned int color) const
{
	HANDLE hConsole;
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, color);
}