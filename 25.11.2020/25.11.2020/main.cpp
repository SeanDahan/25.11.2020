#include "Menu.h"
#include <iostream>

void main()
{

	
	Menu mnu;

	while (true) 
	{
		
			unsigned short choice = mnu.displayMenu();
			
			switch (choice) 
			{
				case mnu.addChoice: 
					mnu.displayShapeMenu(); 
					break;
				case mnu.modifyChoice: 
					mnu.displayShape(); 
					break;
				case mnu.delChoice: 
					mnu.deleteAll(); 
					break;
				case mnu.exitChoice: 
					exit(0); 
					break;
			}
		
		
	}
}