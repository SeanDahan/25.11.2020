#pragma once
#include "Point.h"
#include "Canvas.h"
#include <string>

#define RED 4
#define GREEN 2
#define PURPLE 5
#define GRAY 8
#define LIGHT_GREEN 10
#define AQUA 11
#define LIGHT_RED 12
#define PINK 13
#define YELLOW 6
#define LIGHT_YELLOW 14
#define LIGHT_BLUE 9
#define WHITE 15

class Shape 
{
public:
	Shape(const std::string& name, const std::string& type);
	virtual double getArea() const = 0;
	virtual double getPerimeter() const = 0;
	virtual void draw(const Canvas& canvas) = 0;
	virtual void move(const Point& other) = 0; // add the Point to all the points of shape
	void printDetails() const;
	std::string getType() const;
	std::string getName() const;

	void setColor(unsigned int color) const;

	virtual void clearDraw(const Canvas& canvas) = 0;

private:
	std::string _name;
	std::string _type;
};